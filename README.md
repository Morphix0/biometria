Temat: Sygnały medyczne w biometrii np. EEG, EKG.
Jak rozpoznać człowieka po sygnałach mózgowych lub biciu serca?

-----------------------------------------------------

Linki:

https://www.researchgate.net/post/What_is_the_potential_of_Bio-signal_EEG_and_ECGl_as_biometrics

https://books.google.pl/books?id=fefutm-Dhy0C&pg=PA464&lpg=PA464&dq=eeg+ecg+in+biometry&source=bl&ots=evbTBi67ei&sig=AhZ9Z5uynO5ZBvcbioN3nlxfrr4&hl=en&sa=X&ved=0ahUKEwigwIeS9crNAhVoJJoKHRUTAxQQ6AEIPTAE#v=onepage&q&f=false

https://www.researchgate.net/post/What_is_the_potential_of_Bio-signal_EEG_and_ECGl_as_biometrics

http://www.physionet.org/pn3/ecgiddb/biometric.shtml

http://www.cs.put.poznan.pl/swilk/pmwiki/uploads/Dydaktyka/IwM-PP1-Sygnaly-i-obrazy.pdf

---------------------------------------------------

Podpunkty:

1. Sygnały medyczne - co to, typy sygnałów (tabelka) - Kamil
2. Rozpoznawanie mowy - Karol
3. EKG - Kamil
4. ECG - Karol
5. Błędy biometryczne - Kamil
6. Zastosowanie - Karol

-------------------

Spotkania:
Merging: wtorek, awar. środa
